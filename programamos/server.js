const express = require('express');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const server = express();
//configuración de middleware para aceptar json en su formato

server.use(express.json()); //for parsing application/json
server.use(express.urlencoded( { extended: true })) //for parsin application/x- www-form-urlencoded



// configurar swagger options

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Vehiculos API',
            version: '1.0.0',
        }        
    },
    apis:['./server.js']
};

// Configurar swaggerDocs

const swaggerDocs = swaggerJsDoc(swaggerOptions);

// configuro el server para el uso del swagger o configurar el endpoint

server.use('/api-docs',
            swaggerUI.serve,
            swaggerUI.setup(swaggerDocs));


// array de vehiculos

let vehiculos = [
    {
        marca:"Ford",
        modelo:"Ford EcoSport.",
        fecha_fabricacion: new Date(25/10/2000),
        puertas: 4,
        disponible:true
    },
    {
        marca:"Mazda",
        modelo:"Mazda 3.",
        fecha_fabricacion: new Date(2/05/2015),
        puertas: 5,
        disponible:false
    }
];

// listado de vehiculos

/**
 * @swagger
 * /listado_vehiculos:
 *  get:
 *    description: Listado de vehiculos
 *    responses:
 *      200:
 *        description: Success
 */

server.get('/vehiculos', (req, res) => {
    res.status(201).send(vehiculos);
});

/**
 * @swagger
 * /crear_vehiculo:
 *  post:
 *    description: cargar nuevo vehiculo
 *    responses:
 *      200:
 *        description: Success
 */

 server.post('/vehiculos', (req, res) => {
    res.status(201).send('nuevo vehiculo');
});

//falta agregar PUT y DELETE





server.listen(3000, function() {
    console.log('corriendo puerto 3000 en vehiculos');
});