const express = require('express');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const server = express();

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Acamica API',
            version: '1.0.0',
        }
    },
    apis:['./*.js']
};

//server.use('api-docs', swaggerUI.serve, swaggerUI.setup(SwaggerDocs));

// documentando endpoints //

/**
 * @swagger
 *  /estudiantes:
 *  post:
 *      description: Crea un nuevo estudiante
 *      parameters: 
 *      - name: nombre
 *          description: Nombre del estudiante
 *          in: formData
 *          required: true
 *          type: string
 *      - name: edad
 * 
 *      responses:
 *          200:
 *                
 * 
 */

server.get('/estudiantes', (req, res) => {
    res.status(201).send('listado');
});












app.listen(3000, function () { 
    console.log("corriendo en puerto 3000 swagger")   
});