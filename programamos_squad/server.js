const express = require('express');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const server = express();
//configuración de middleware para aceptar json en su formato

server.use(express.json()); //for parsing application/json
server.use(express.urlencoded( { extended: true })) //for parsin application/x- www-form-urlencoded


// configurar swagger options

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'foro API',
            version: '1.0.0',
        }        
    },
    apis:['./server.js']
};

// Configurar swaggerDocs

const swaggerDocs = swaggerJsDoc(swaggerOptions);

// configuro el server para el uso del swagger o configurar el endpoint

server.use('/api-docs',
            swaggerUI.serve,
            swaggerUI.setup(swaggerDocs));


let usuarios = [
    {
        id: 1,
        nombre: 'Juan',
        apellido: 'Perez',
        email: 'juan@mail.com',
    }
];

let topicos = [
    {
        id: 1,
        titulo: 'Desarrollo en JS',
        descripcion: 'Foro sobre desarrollo en JavaScript',
    }
]

let comentarios = [
    {
        id: 1,
        id_topico: topicos[0].id,
        id_usuario: usuarios[0].id,
        comentarios: 'Esto es un comentario del tópico 1',
    }
]

// endpoint usuarios

/**
 * @swagger
 * /usuarios:
 *  get:
 *    description: Listado de usuarios
 *    parameters:
 *    - name: nombre
 *      description: Nombre del usuario 
 *      required: true
 *      type: string
 *    - name: apellido
 *      description: Apellido del usuario
 *      required: true
 *      type: string
 *    - name: email
 *      description: email del usuario
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */

server.get('/usuarios', (req, res) => {
    res.status(201).send(usuarios);
});


/**
 * @swagger
 * /usuarios:
 *  post:
 *    description: Listado de usuarios
 *    parameters:
 *    - name: nombre
 *      description: Nombre del usuario 
 *      required: true
 *      type: string
 *    - name: apellido
 *      description: Apellido del usuario
 *      required: true
 *      type: string
 *    - name: email
 *      description: email del usuario
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */


server.post('/usuarios', (req, res) => {
    const {id, nombre, apellido, email} = req.body;
    const usuario = {
        id: id,
        nombre: nombre,
        apellido: apellido,
        email: email,
    };
    usuarios.push(usuario);

    //return res.send(usuarios);
    res.status(201).send('Nuevo usuario');
    console.log(usuarios);
});





server.listen(3000, function() {
    console.log('corriendo puerto 3000 en vehiculos');
});